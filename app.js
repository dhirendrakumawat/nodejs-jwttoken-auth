const express =require('express');
const app =express();
const bodyparser =require('body-parser');
app.set('port', process.env.PORT || 35000);
var port =app.get('port');
var jsonParser=bodyparser.json();
const jwt =require('jsonwebtoken');
var jwtkey = 'jwt';
const Users = require('./models/user');
const PasswordCategory = require('./models/password_category');
const AddPassword = require('./models/add_password');
var mongoose = require('mongoose');
const uri = "mongodb+srv://nodeuser:node@123@cluster0.xverq.mongodb.net/node123?retryWrites=true&w=majority";
mongoose.connect(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
.then(() => {
  console.log('Connected…')
})
.catch(err => console.log(err))
app.get('/',(req, res)=> {
    res.status(200).send('API works.');
  });

// import routes
const user = require("./routes/user");
const Passwordcateroutes = require("./routes/passwordcategory");
// route middlewares
app.use("/api", user);
app.use("/api", Passwordcateroutes);

app.listen(port,console.warn('Server is running....'));