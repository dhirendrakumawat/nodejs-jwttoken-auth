const express =require('express');
const jwt =require('jsonwebtoken');
var jwtkey = 'jwt';


//Middleware   verify Token 
function verifyToken(req,res,next){
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader!='undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    req.token = bearerToken;
    jwt.verify(req.token,jwtkey,(err,authdata)=>{
        if(err){
            res.json({result:err});
        }else{
          next();
        }
    })  
    
  } else {
    // Forbidden
    res.sendStatus(403).json('Authentication error');
  }
}

module.exports = verifyToken;
