const express =require('express');
var router =express.Router();
const crypto =require('crypto');
var key ='password';
var algo ='aes192';
//Body Parser Middleware
const bodyparser =require('body-parser');
var jsonParser=bodyparser.json();
// Import Jwt Token
const jwt =require('jsonwebtoken');
var jwtkey = 'jwt';
//User models import
const AddPassword   = require('../models/add_password');
//import middleware
var verifyToken = require('../middleware/verifytoken');
//register api
  router.post('/add-password-detail',jsonParser,verifyToken,function(req,res){
    var data =new AddPassword({
        password_category:req.body.password_category,
        project_name:req.body.project_name,
        projectuser:req.body.projectuser,
        Projectpassword:req.body.Projectpassword
     });

     data.save().then((result)=>{
    res.status(201).json({result});
 
  });
});

//Get  All User By Verify Token
router.get('/password-detail',verifyToken, function(req,res){
    AddPassword.find().then((result)=>{
      res.status(200).json(result);
  })
  });


  module.exports = router;