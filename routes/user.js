const express =require('express');
var router =express.Router();
const crypto =require('crypto');
var key ='password';
var algo ='aes192';
//Body Parser Middleware
const bodyparser =require('body-parser');
var jsonParser=bodyparser.json();
// Import Jwt Token
const jwt =require('jsonwebtoken');
var jwtkey = 'jwt';
//User models import
const User = require('../models/user');
//import middleware
var verifyToken = require('../middleware/verifytoken');
//register api
  router.post('/register',jsonParser,function(req,res){
    var cipher=crypto.createCipher(algo,key);
    var encrypted =cipher.update(req.body.password,'utf8','hex');
    encrypted+=cipher.final('hex');
    //console.log(encrypted);
    var data =new User({
       name:req.body.name,
   email:req.body.email,
   address:req.body.address,
   password:encrypted
    });
    data.save().then((result)=>{
        jwt.sign({result},jwtkey,{expiresIn:'300s'},(err,token)=>{
   res.status(201).json({user:result,token});
})
    }).catch((err)=>console.warn(err))
   });
//login api
router.post('/login',jsonParser,(req,res)=>{
    User.findOne({email:req.body.email}).then((data)=>{
        var decipher=crypto.createDecipher(algo,key);
        var decrypted =decipher.update(data.password,'hex','utf8');
        decrypted=decipher.final('utf8');
        if(decrypted==req.body.password){
            jwt.sign({data},jwtkey,{expiresIn:'300s'},(err,token)=>{
                res.status(200).json({token});
            })
        }
    })
});
//Get  All User By Verify Token
router.get('/users',verifyToken, function(req,res){
    User.find().then((result)=>{
        res.status(200).json(result);
    })
    });
  module.exports = router;